﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;


// Changes which layer the player currently is on
//  (If the player is shown in front or bakc an objcet)
public class PlayerLayer : MonoBehaviour
{

    public string layerName;
    public int sortingOrder = 2;

    private Rigidbody2D rb;
    private SpriteRenderer sprite;

   private Scene currentScene;


    void Start()
    {
        rb = GetComponent<Rigidbody2D>();
        sprite = GetComponent<SpriteRenderer>();
        currentScene = SceneManager.GetActiveScene();
    }

    void Update()
    {
        if (sprite)
        {
            string sceneName = currentScene.name;
            double yPos = rb.position.y;

            double layer2 = 0;
            double layer1 = 0;

          //  layer2 = -3.139;
            //layer1 = -1.4;


            if (sceneName == "MidTown")
            {
                layer2 = -3.139;
                layer1 = -1.4;
            }
            else if (sceneName == "HousHospital")
            {
                layer2 = 0.6;
                layer1 = 1.17;
            }

            if (yPos <= layer2)
            {
                layerName = "Second Layer";
            }
            else if (yPos <= layer1)
            {
                layerName = "First Layer";
            }
            else
            {
                layerName = "Background";
            }



            sprite.sortingOrder = sortingOrder;
            sprite.sortingLayerName = layerName;
        }
    }
}