﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


// Limits the camera so it can't see "outside" the scene, 
//  and/or follows the player
public class CameraController : MonoBehaviour
{
    public Transform Player;

    public Vector2 Margin;
    public Vector2 Smoothing;

    public BoxCollider2D Bounds;

    private Vector3 _min;
    private Vector3 _max;

    public bool isFollow { get; set; }


    public void Start ()
    {
        _min = Bounds.bounds.min;
        _max = Bounds.bounds.max;
        isFollow = true;
    }

    public void Update()
    {
        var x = transform.position.x;
        var y = transform.position.y;

        if(isFollow)
        {
            if(Mathf.Abs(x - Player.position.x) > Margin.x)
            {
                x = Mathf.Lerp(x, Player.position.x, Smoothing.x * Time.deltaTime);
            }

            if (Mathf.Abs(y - Player.position.y) > Margin.y)
            {
                y = Mathf.Lerp(y, Player.position.y, Smoothing.y * Time.deltaTime);
            }
        }

        var cameraHalfWidth = GetComponent<Camera>().orthographicSize * ((float)Screen.width / Screen.height);

        x = Mathf.Clamp(x, _min.x + cameraHalfWidth, _max.x - cameraHalfWidth);
        y = Mathf.Clamp(y, _min.y + GetComponent<Camera>().orthographicSize, _max.y - GetComponent<Camera>().orthographicSize);

        transform.position = new Vector3(x, y, transform.position.z);

    }

}