﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


// Places the player around the "world" based on previous scene
public class World : SceneController
{

    public Transform player;

    // Use this for initialization
    public override void Start()
    {
        base.Start();

        if (prevScene == "HousHospital")
        {
            player.position = new Vector2(1.73f, -1.56f);
            Camera.main.transform.position = new Vector3(1.73f, -1.56f, -10f);
        }
    }

}
