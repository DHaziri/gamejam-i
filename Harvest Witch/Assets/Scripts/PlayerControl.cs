﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


// Controls player
public class PlayerControl : MonoBehaviour
{
    public float moveSpeed;
    private Rigidbody2D rb;

    private Vector2 moveVelocity;

    void Start ()
    {
        rb = GetComponent<Rigidbody2D>();
    }

    void Update ()
    {
        Vector2 moveInput = new Vector2(Input.GetAxis("Horizontal"), Input.GetAxis("Vertical"));
        moveVelocity = moveInput.normalized * moveSpeed;

    }

    void FixedUpdate()
    {
        rb.MovePosition(rb.position + moveVelocity * Time.fixedDeltaTime);
    }
}

